"use strict";

const port = 80;
const baseurl = '/';
const endpoint = '/IBLCashIn/ConfirmPayment';
const invoke = 'https://api-uat.unionbankph.com/ubp/sb/IBLCashIn/ConfirmPayment';

const express = require('express');
const server = express();
const bodyParser = require('body-parser');

server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

const router = express.Router();

router.post(endpoint, (req, res) => {

	const request = require('request');
	const request_settings = {
		"method" : "post",
		"url" : invoke,
		"headers" : req.headers,
		"body" : req.body,
		"json" : true
	};

	request(request_settings, (err, resp, body) => {
		if(err) {
			res.json(err);	
		}	
		else {
			res.json(body);
		}
	});
	
});

server.use(baseurl, router);
server.listen(port, () => {
	console.log('Listening on port ' + port);
});
